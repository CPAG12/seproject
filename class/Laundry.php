<?php
require_once('../database/Database.php');
require_once('../interface/iLaundry.php');
class Laundry extends Database implements iLaundry {

	public function insert_laundry($type, $price)
	{
		$sql = "INSERT INTO laundry_type (laun_type_desc, laun_type_price)
				VALUES(?,?)";
		return $this->insertRow($sql, [$type, $price]);
	}//end insert_laundry

	public function get_all_laundry()
	{
		$sql = "SELECT *
				FROM laundry_type
				ORDER BY laun_type_desc ASC";
		return $this->getRows($sql);
	}//end get_all_laundry

	public function get_all_type()
	{
		$sql = "SELECT *
				FROM laundry_type
				ORDER BY laun_type_desc ASC";
		return $this->getRows($sql);
	}//end get_all_laundry

	public function get_type($type_id)
	{
		$sql = "SELECT * 
				FROM laundry_type
				WHERE laun_type_id = ?";
		return $this->getRow($sql, [$type_id]);
	}//end get_type

	public function edit_type($type_id, $type, $price)
	{
		$sql = "UPDATE laundry_type
				SET laun_type_desc = ?, laun_type_price = ?
				WHERE laun_type_id = ?";
		return $this->updateRow($sql, [$type, $price, $type_id]);
	}//end edit_type

	public function all_laundry()
	{
		$claimed = 0;//zero means wala pa
		$sql = "SELECT *
				FROM laundries l
				INNER JOIN laundry_type lt
				ON l.laun_type_id = lt.laun_type_id
				WHERE l.laun_claimed = ?
				ORDER BY customer_name ASC";
		return $this->getRows($sql, [$claimed]);
	}//end all_laundry

	public function new_laundry($customer, $priority, $weight, $type)
	{
		$sql = "INSERT INTO laundries(customer_name, laun_priority, laun_weight, laun_type_id)
				VALUES(?,?,?,?);";
		return $this->insertRow($sql, [$customer, $priority, $weight, $type]);
	}//end new_laundry

	public function delete_laundry($laun_id)
	{
		$sql = "DELETE FROM laundries
				WHERE laun_id = ?";
		return $this->deleteRow($sql, [$laun_id]);
	}//end delete_laundry

	public function get_laundry($laun_id)
	{
		$sql = "SELECT *
				FROM laundries
				WHERE laun_id = ?";
		return $this->getRow($sql, [$laun_id]);
	}//end get_laundry

	public function edit_laundry($laun_id, $customer, $priority, $weight, $type)
	{
		$sql = "UPDATE laundry 
				SET customer_name = ?, laun_priority = ?, laun_weight = ?, laun_type_id = ?
				WHERE laun_id = ?";
		return $this->updateRow($sql, [$customer, $priority, $weight, $type, $laun_id]);
	}//end edit_laundry

	public function get_laundry2($laun_id)
	{
		$sql = "SELECT *
				FROM laundries l 
				INNER JOIN laundry_type lt 
				ON l.laun_type_id = lt.laun_type_id
				WHERE l.laun_id = ?";
		return $this->getRow($sql, [$laun_id]);
	}//end get_laundry

	public function claim_laundry($laun_id, $amount)
	{
		$claimed = 1;//1 means ge claim na.. dili na e display sa table laundry
		$sql = "UPDATE laundries 
				SET laun_claimed = 1 , laun_amount = ?
				WHERE laun_id = ?";
		return $this->updateRow($sql, [$amount, $laun_id]);
	}//end claim_laundry

	public function daily_sales($date)
	{	
		$sql = "SELECT *
				FROM laundries l
				INNER JOIN laundry_type lt 
				ON l.laun_type_id = lt.laun_type_id
				WHERE DATE(`laun_date_paid`) = ?
				ORDER BY laun_date_paid DESC";
		return $this->getRows($sql, [$date]);
	}//end daily_sales


	public function get_all_customers()
	{
		$sql = "SELECT *
				FROM customers
				ORDER BY name ASC";
		return $this->getRows($sql);
	}//end get_all_customers

	public function edit_customer($customer_id, $cName, $cContact, $cAddress, $cEmail)
	{
		$sql = "UPDATE customers
				SET name = ?, contact = ?, address = ?, email = ?
				WHERE customer_id = ?";
		return $this->updateRow($sql, [$cName, $cContact, $cAddress, $cEmail, $customer_id]);
	}//end edit_customer

	public function get_customer($customer_id)
	{
		$sql = "SELECT * 
				FROM customers
				WHERE customer_id = ?";
		return $this->getRow($sql, [$customer_id]);
	}//end get_customer

	public function new_customer($cName, $cContact, $cAddress, $cEmail)
	{
		$sql = "INSERT INTO customers(name, contact, address, email)
				VALUES(?,?,?,?);";
		return $this->insertRow($sql, [$cName, $cContact, $cAddress, $cEmail]);
	}//end new_laundry

	public function delete_customer($customer_id)
	{
		$sql = "DELETE FROM customers
				WHERE customer_id = ?";
		return $this->deleteRow($sql, [$customer_id]);
	}//end delete_customer

	public function all_customers()
	{
		$sql = "SELECT *
				FROM customers
				ORDER BY name ASC";
		return $this->getRows($sql);
	}//end all_laundry


}//end class
$laundry = new Laundry();
/* End of file Laundry.php */
/* Location: .//D/xampp/htdocs/laundry/class/Laundry.php */
