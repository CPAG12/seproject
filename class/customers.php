<?php
require_once('../database/Database.php');
require_once('../interface/iCustomers.php');
class customers extends Database implements iCustomers {

	public function insert_customer($cName, $cContact, $cAddress, $cEmail)
	{
		$sql = "INSERT INTO customers (name, contact, address, email)
				VALUES(?,?,?,?)";
		return $this->insertRow($sql, [$cName, $cContact, $cAddress, $cEmail]);
	}//end insert_customer

	public function get_all_customers()
	{
		$sql = "SELECT *
				FROM customers
				ORDER BY name ASC";
		return $this->getRows($sql);
	}//end get_all_customers

	public function get_customer($customer_id)
	{
		$sql = "SELECT * 
				FROM customers
				WHERE customer_id = ?";
		return $this->getRow($sql, [$customer_id]);
	}//end get_customer

	public function edit_customer($customer_id, $cName, $cContact, $cAddress, $cEmail)
	{
		$sql = "UPDATE customers
				SET name = ?, contact = ?, address = ?, email = ?
				WHERE customer_id = ?";
		return $this->updateRow($sql, [$cName, $cContact, $cAddress, $cEmail, $customer_id]);
	}//end edit_type


	public function new_customer($name, $contact, $address, $email)
	{
		$sql = "INSERT INTO customers(name, contact, address, email)
				VALUES(?,?,?,?);";
		return $this->insertRow($sql, [$name, $contact, $address, $email]);
	}//end new_customer

	public function delete_customer($customer_id)
	{
		$sql = "DELETE FROM customers
				WHERE customer_id = ?";
		return $this->deleteRow($sql, [$customer_id]);
	}//end delete_customer

}//end class
$customer = new customers();
/* End of file customers.php */
/* Location: .//D/xampp/htdocs/laundry/class/Laundry.php */