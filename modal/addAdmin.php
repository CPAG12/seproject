<div class="modal fade" id="modal-addAdmin">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" id="form-addAdmin">
					<input type="hidden" id="admin-type" value="insert">
					<input type="hidden" id="admin-id">
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="userName">Username:</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="userName" placeholder="Enter Username" required="">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="pwd">Passowrd:</label>
				    <div class="col-sm-10"> 
				      <input type="password" class="form-control" id="pwd" placeholder="Enter Password" required="">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="pwd2">Confirm:</label>
				    <div class="col-sm-10"> 
				      <input type="password" class="form-control" id="pwd2" placeholder="Confirm Password" required="">
				    </div>
				  </div>
				  
				  <div class="form-group"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-success">Add Admin</button>
				    </div>
				  </div>
				</form>				
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>