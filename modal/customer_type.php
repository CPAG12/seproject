<div class="modal fade" id="modal-new-customer">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">New Customer</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" id="form-customer">
					<input type="hidden" id="customer-type" value="insert">
					<input type="hidden" id="customer-id">
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Name:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="cName" placeholder="Enter Full Name" required>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Contact:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="cContact" placeholder="Enter Contact" required>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Address:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="cAddress" placeholder="Enter Address" required>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Email:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="cEmail" placeholder="Enter Email Address" required>
				    </div>
				  </div> 
				  <div class="form-group"> 
				    <div class="col-sm-offset-3 col-sm-9">
				      <button type="submit" class="btn btn-success">Save</button>
				    </div>
				  </div>
				</form>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>