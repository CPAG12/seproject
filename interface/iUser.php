<?php 
interface iUser{
	public function login($username, $password);
	public function change_pass($pwd, $uid);
	public function add_admin($userName, $pwd);
}//end iUser