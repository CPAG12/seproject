<?php 
interface iCustomers{
	public function insert_customer($name, $contact, $address, $email);
	public function get_all_customers();//all customers
	public function get_customer($customer_id);
	public function edit_customer($customer_id, $name, $contact, $address, $email);
	public function new_customer($name, $contact, $address, $email);
	public function delete_customer($customer_id);
}//end iCustomer