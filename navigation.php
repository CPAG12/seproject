  <li class="header">Laundry</li>
           
    <li class="treeview">
      <a href="home.php">
        <i class="fa fa-home"></i>
        <span>Home</span>
      </a>
    </li>

    <li class="treeview">
      <a href="laundrytype.php">
        <i class="fa fa-th-list"></i>
        <span>Laundry Type</span>
      </a>
    </li>

    <li class="treeview">
      <a href="customersList.php">
        <i class="fa fa-users"></i>
        <span>Customers</span>
      </a>
    </li> 

    <li class="treeview">
      <a href="report.php">
        <i class="fa fa-print"></i>
        <span>Report</span>
      </a>
    </li>

<li class="header">SETTINGS</li>

  <li><a id="addAdmin" href="#"><i class="fa fa-circle-o text-blue"></i> 
    <span>Add New Admin</span></a>
  </li>

	<li><a id="changePass" href="#"><i class="fa fa-circle-o text-yellow"></i> 
		<span>Change Password</span></a>
	</li>

	<li><a href="logout.php"><i class="fa fa-circle-o text-red"></i> 
		<span>Logout</span></a>
	</li>