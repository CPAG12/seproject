<?php 
require_once('../class/Laundry.php');
if(isset($_POST['customer_id'])){
	$customer_id = $_POST['customer_id'];
	$cName = $_POST['cName'];
	$cContact = $_POST['cContact'];
	$cAddress = $_POST['cAddress'];
	$cEmail = $_POST['cEmail'];
	$cName = strtolower($cName);
	$cName = ucwords($cName);

	$updateRecord = $laundry->edit_customer($customer_id, $cName, $cContact, $cAddress, $cEmail);
	$return['valid'] = false;
	if($updateRecord){
		$return['valid'] = true;
		$return['msg'] = 'Edit Successfully!';
	}
	echo json_encode($return);
}//end isset
$laundry->Disconnect();