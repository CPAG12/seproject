<?php 
require_once('../class/Laundry.php');
if(isset($_POST['cName'])){
	$cName = $_POST['cName'];
	$cContact = $_POST['cContact'];
	$cAddress = $_POST['cAddress'];
	$cEmail = $_POST['cEmail'];


	$saveCustomer = $laundry->new_customer($cName, $cContact, $cAddress, $cEmail);
	$return['valid'] = false;
	if($saveCustomer){
		$return['valid'] = true;
		$return['msg'] = 'New Customer Added Successfully!';
	}
	echo json_encode($return);
}//end isset
$laundry->Disconnect();