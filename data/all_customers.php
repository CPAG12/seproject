<br />
<?php 
require_once('../class/Laundry.php');
$types = $laundry->get_all_customers();
 ?>

<div class="table-responsive">
        <table id="myTable-customers" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th></th>   
                    <th><center>Name</center></th>
                    <th><center>Contact</center></th>
                    <th><center>Address</center></th>
                    <th><center>Email</center></th>
                    <th><center>Action</center></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($types as $t): ?>
                    <tr align="center">
                        <td><input type="checkbox" name="imSlepy" value="<?= $t['customer_id']; ?>"></td>
                        <td align="left"><?= ucwords($t['name']); ?></td>
                        <td align="center"><?= $t['contact']; ?></td>
                        <td align="center"><?= $t['address']; ?></td>
                        <td align="center"><?= $t['email']; ?></td>
                        <td>
                            <button onclick="editCustomer('<?= $t['customer_id']; ?>');" type="button" class="btn btn-warning btn-xs">Edit
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </button>   
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
</div>


<!-- for the datatable of employee -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable-customers').DataTable();
    });
</script>

<?php $laundry->Disconnect(); ?>