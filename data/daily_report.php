<?php 
require_once('../class/Laundry.php');
if(isset($_POST['date'])){
	$date = $_POST['date'];

	$reports = $laundry->daily_sales($date);
	// echo '<pre>';
	// 	print_r($reports);
	// echo '</pre>';
    $getPrice = $laundry->all_laundry();
?>
<br />
<div class="table-responsive">
        <table id="myTable-report" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th><center>Type</center></th>
                    <th><center>Weight</center></th>
                    <th><center>Laundry Received</center></th>
                    <th><center>Amount</center></th>
                </tr>
            </thead>
            <tbody>
            	<?php 
            		$total = 0;
                    $launPrice = 0;

            		foreach($reports as $r):
                    
                    $launWeight = $r['laun_weight'] * $r['laun_type_price'];
            		$total += $launWeight;
            	?>
	                <tr align="center">
	                    <td align="left"><?= $r['customer_name']; ?></td>
	                    <td><?= $r['laun_type_desc']; ?></td>
                        <td><?= $r['laun_weight']; ?></td>
	                    <td><?= $r['laun_date_received']; ?></td>
	                    <td><?= '₱ '.number_format($launWeight, 2); ?></td>
	                </tr>
	            <?php endforeach; ?>
            </tbody>
	            <tr>
	            	<td></td>
	            	<td></td>
	            	<td></td>
                    <td></td>
	            	<td align="right"><strong>TOTAL:</strong></td>
	            	<td align="center"><strong><?= '₱ '.number_format($total,2); ?></strong></td>
	            </tr>
        </table>
</div>


<!-- for the datatable of employee -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable-report').DataTable();
    });
</script>



<?php
}//end isset


